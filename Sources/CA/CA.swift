//
//  CA.swift
//
//  Created by Matthew Burke on 1/24/20.
//  Copyright © 2020 Bluedino Software. All rights reserved.
//

// Bare-bones implementation of Conway's life.
// TBA: slight elaboration: 4 color life:
// see https://conwaylife.com/ref/mniemiec/color.htm
import Foundation

extension Array {
    subscript(index: CA.CellState) -> Element {
        return self[index.rawValue]
    }
}

// TODO: make CA generic on CellState
// TODO: also allow other options in addition to standard Von Neumann neighborhood
public class CA {
    // TODO: use alive(int) instead of four states...
    // if so, how do we limit to a particular range
    public enum CellState: Int, CaseIterable {
        case dead = 0
        case alive1
        case alive2
        case alive3
        case alive4

        public var isAlive: Bool {
            return self != .dead
        }
    }

    public let rows: Int
    public let columns: Int

    // TODO: would it be useful to make current and next an enum?
    private var currentGrid = 0
    private var nextGrid = 1

    private var world: [[[CellState]]]

    private func neighbors(_ r: Int, _ c: Int) -> [Int] {
        var n: [Int] = Array(repeating: 0, count: 5)

        for dr in -1...1 {
            for dc in -1...1 {
                if (dr == 0) && (dc == 0) { continue }
                if (r + dr < 0) || (r + dr >= rows) { continue }
                if (c + dc < 0) || (c + dc >= columns) { continue }
                let current = get(r + dr, c + dc)
                n[current.rawValue] += 1
            }
        }

        return n
    }

    // assuming that total number of neighbors is 3 (otherwise there's no birth...)
    private func getNewState(_ n: [Int]) -> CellState {
        if n[CellState.alive1] > 1 { return CellState.alive1 }
        if n[CellState.alive2] > 1 { return CellState.alive2 }
        if n[CellState.alive3] > 1 { return CellState.alive3 }
        if n[CellState.alive4] > 1 { return CellState.alive4 }

        // no majority so return the missing color
        if n[CellState.alive1] == 0 { return CellState.alive1 }
        if n[CellState.alive2] == 0 { return CellState.alive2 }
        if n[CellState.alive3] == 0 { return CellState.alive3 }
        if n[CellState.alive4] == 0 { return CellState.alive4 }

        // shouldn't be able to get here, but ...
        return .dead
    }

    private func newvalue(_ r: Int, _ c: Int, _ n: [Int]) {
        let curval = get(r, c)
        let population = n[CellState.alive1.rawValue] + n[CellState.alive2.rawValue]
        + n[CellState.alive3.rawValue] + n[CellState.alive4.rawValue]

        if curval == .dead {
            if population == 3 {
                let newValue = getNewState(n)
                set(r, c, newValue)
            } else {
                set(r, c, .dead)
            }
        } else {
            if population == 2 || population == 3 {
                set(r, c, curval)
            } else {
                set(r, c, .dead)
            }
        }
    }

    private func swapGrids() {
        (currentGrid, nextGrid) = (nextGrid, currentGrid)
    }

    public func step() {
        for r in 0..<rows {
            for c in 0..<columns {
                let n = neighbors(r, c)
                newvalue(r, c, n)
            }
        }

        swapGrids()
    }

    public func get(_ r: Int, _ c: Int) -> CellState {
        return world[currentGrid][r][c]
    }

    public func set(_ r: Int, _ c: Int, _ v: CellState) {
        world[nextGrid][r][c] = v
    }

    public init(_ rows: Int, _ columns: Int) {
        self.rows = rows
        self.columns = columns
        world = Array(repeating: Array(repeating: Array(repeating: .dead, count: columns), count: rows), count: 2)
    }
}

extension CA {
    public func initGrid(_ valgen: (Int, Int) -> CA.CellState) {
        for r in 0..<rows {
            for c in 0..<columns {
                set(r, c, valgen(r, c))
            }
        }
        swapGrids()
    }

    // TODO: add a parameter to adjust away from 50-50
    public func random() {
        initGrid({ _, _ in
            let coin = Int.random(in: 0...1)
            if coin == 0 {
                return .dead
            } else {
                return CA.CellState.allCases.randomElement()!
            }
        })
    }
}

extension CA: CustomStringConvertible {
    // TODO: we're collapsing all the alive states to one symbol, probably not do this ???
    public var description: String {
        var result = ""

        for r in 0..<rows {
            for c in 0..<columns {
                result += "\(get(r, c).isAlive ? "*" : " ")"
            }
            result += "\n"
        }

        return result
    }
}
