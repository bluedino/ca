import XCTest

import CATests

var tests = [XCTestCaseEntry]()
tests += CATests.allTests()
XCTMain(tests)
