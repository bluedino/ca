// swift-tools-version:5.1
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "CA",
    products: [
        .library(
            name: "CA",
            targets: ["CA"]),
    ],
    dependencies: [],
    targets: [
        .target(
            name: "CA",
            dependencies: []),
        .testTarget(
            name: "CATests",
            dependencies: ["CA"]),
    ]
)
